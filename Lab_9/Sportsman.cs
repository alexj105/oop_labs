﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Lab_9
{
    enum Sport
    {
        Бокс = 0, Футбол = 1, Стрельба= 2, Биатлон = 3
    }

    enum Category
    {
        I, II, III, IV
    }

    
    struct Sportsman
    {
        string fio;
        int year;
        Sport typeSport;
        Category typeCategory;

        public string Fio
        {
            get { return fio; }
            set { fio = value; }
        }

        public int Year
        {
            get { return year; }
            set { year = value; }
        }

        public Sport TypeSport
        {
            get { return typeSport; }
            set { typeSport = value; }
        }

        public Category TypeCategory
        {
            get { return typeCategory; }
            set { typeCategory = value; }
        }

        public void OutputToScreen(int num)
        {
            Console.WriteLine("│{0,3}│{1,19}│{2,13}│{3,17}│",num,this.Fio,2013-this.Year,this.TypeCategory);
        }

        public void OutputToFile(int num,StreamWriter F)
        {
            F.WriteLine("│{0,3}│{1,19}│{2,13}│", num, this.Fio, 2013 - this.Year);
        }
    }
}
