﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Lab_9
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader f = WorkWithFile.OpenFile();
            List<string> data = WorkWithFile.ReadFromFile(f);
            Sportsman[] sportsmans = SportsmanHelper.CreateArrayOfSportsmans(data);
            
            Sport type = SportsmanHelper.Menu();

            SportsmanHelper.OutputSportsmansOfType(sportsmans, type);

            WorkWithFile.PrintInAFileAllCategories(sportsmans);

            Console.WriteLine("\nДля выхода нажмите любую клавишу . . . ");
            Console.ReadKey();
        }
    }
}
