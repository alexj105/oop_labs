﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab_9
{
    class SportsmanHelper
    {
        /// <summary>
        /// Возвращает массив структур Sportsman.
        /// </summary>
        /// <param name="L">Список строк с информацией.</param>
        /// <returns></returns>
        public static Sportsman[] CreateArrayOfSportsmans(List<string> L)
        {
            Sportsman[] res = new Sportsman[L.Count];

            for (int i = 0; i < L.Count; i++)
            {
                string[] temp = L[i].Split(';');

                res[i].Fio = temp[0];
                res[i].Year = Convert.ToInt32(temp[1]);

                res[i].TypeSport = (Sport)Enum.Parse(typeof(Sport), temp[2]);// приводим строку к перечислению а затем приводим её к типу Sport
                res[i].TypeCategory = (Category)Enum.Parse(typeof(Category), temp[3]);// приводим строку к перечислению а затем приводим её к типу Category

            }

            return res;
        }

        /// <summary>
        /// Функция для выбора вида спорта.
        /// </summary>
        /// <returns></returns>
        public static Sport Menu()
        {
            Sport temp = (Sport)0;
            
            bool exit = false;
            while (!exit)
            {
                Console.Clear();
                Console.WriteLine("Выберите вид спорта:");
                for (int i = 0; i < 4; i++)
                {
                    if (i == (int)temp)
                    {
                        Console.BackgroundColor = ConsoleColor.DarkGreen;
                        Console.WriteLine((Sport)i);
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.WriteLine((Sport)i);
                    }
                }
                ConsoleKeyInfo button = Console.ReadKey();
                switch (button.Key)
                {
                    case ConsoleKey.UpArrow:
                        if ((int)temp == 0)
                            temp = (Sport)3;
                        else
                            temp--;
                        break;
                    case ConsoleKey.DownArrow:
                        if ((int)temp == 3)
                            temp = (Sport)0;
                        else
                            temp++;
                        break;
                    case ConsoleKey.Enter:
                        exit = true;
                        break;
                }
            }
            Console.WriteLine("\nВид спорта выбран.");
            Console.WriteLine("Для продолжения нажмите любую клавишу . . . ");
            Console.ReadKey();
            Console.WriteLine();
            return temp;
        }


        public static void OutputSportsmansOfType(Sportsman[] sportsmans, Sport type)
        {
            Console.WriteLine("Вид спорта \"{0}\":", type);
            Console.WriteLine("┌───┬───────────────────┬─────────────┬─────────────────┐");
            Console.WriteLine("│ № │      Фамилия      │   Возраст   │    Разряд       │");
            Console.WriteLine("├───┼───────────────────┼─────────────┼─────────────────┤");
            int numInTable = 0;
            for (int i = 0; i < sportsmans.Length; i++)
            {
                if (sportsmans[i].TypeSport == type)
                    sportsmans[i].OutputToScreen(++numInTable);
            }
            Console.WriteLine("└───┴───────────────────┴─────────────┴─────────────────┘\n\n");
        }

        public static bool IsBeSuchSportsman(Category c, Sport s, Sportsman[] sportsmans)
        {
            foreach (Sportsman x in sportsmans)
                if (x.TypeCategory == c && x.TypeSport == s)
                    return true;
            return false;
        }
    }
}
