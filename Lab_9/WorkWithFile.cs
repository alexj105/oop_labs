﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Lab_9
{
    class WorkWithFile
    {
        /// <summary>
        /// Открывает текстовый файл.
        /// </summary>
        /// <returns></returns>
        public static StreamReader OpenFile()
        {
            StreamReader F = null;
            bool exit = false;
            while (!exit)
            {
                try
                {
                    Console.Write("Введите имя файла: ");
                    string nameFile = Console.ReadLine();
                    F = new StreamReader(@nameFile, Encoding.Default);
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Файл с имнем \"" + nameFile + "\" успешно открыт.");
                    Console.ResetColor();
                    exit = true;
                }
                catch (FileNotFoundException)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("\nошибка: Файл с указанным именем не существует!\n");
                    Console.ResetColor();
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("\nошибка: " + e.Message + "\n");
                    Console.ResetColor();
                }
            }
            return F;
        }

        /// <summary>
        /// Создаёт список из строк с информацией для массива структур.
        /// </summary>
        /// <param name="F">Поток(файл), содержащий строки с информацией.</param>
        /// <returns></returns>
        public static List<string> ReadFromFile(StreamReader F)
        {
            List<string> res = new List<string>(); // список из считанных строк(замена массиву)
            string buf;

            while ((buf = F.ReadLine()) != null) // непосредственное считывание из файла
                res.Add(buf);// добавление считываемой строки в список

            F.Close();

            return res;
        }

        /// <summary>
        /// Создаёт новый поток (файл) для записи.
        /// </summary>
        /// <param name="nameFile">Имя создаваемого файла.</param>
        /// <returns></returns>
        public static StreamWriter CreateStreamWriter(string nameFile)
        {
            StreamWriter F = null;
            bool exit = false;
            while (!exit)
            {
                try
                {
                    F = new StreamWriter(nameFile, false, Encoding.Unicode);
                    Console.Write("Файл с имнем \"" + nameFile + "\" успешно создан. ");
                    exit = true;
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("\nошибка: " + e.Message + "\n");
                    Console.ResetColor();
                }
            }
            return F;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="journ">.</param>
        /// <param name="subs">.</param>
        /// <param name="F">.</param>
        public static void PrintInAFileOneCategory(Category temp, Sportsman[] sportsmans, StreamWriter F)
        {
            
            F.WriteLine("Разряд \"{0}\":", temp);
            for (int t = 0; t < 4; t++)
            {
                if (SportsmanHelper.IsBeSuchSportsman(temp, (Sport)t, sportsmans))
                {

                    F.WriteLine("Вид спорта \"{0}\":", (Sport)t);
                    F.WriteLine("┌───┬───────────────────┬─────────────┐");
                    F.WriteLine("│ № │      Фамилия      │   Возраст   │");
                    F.WriteLine("├───┼───────────────────┼─────────────┤");
                    int numInTable = 0;
                    for (int i = 0; i < sportsmans.Length; i++)
                    {
                        if (sportsmans[i].TypeCategory == temp && sportsmans[i].TypeSport == (Sport)t)
                            sportsmans[i].OutputToFile(++numInTable, F);
                    }
                    F.WriteLine("└───┴───────────────────┴─────────────┘");
                    F.WriteLine();
                }
                else
                    F.WriteLine("Спортсменов {0}-го разряда c видом спорта {1} не обнаружено.", temp,(Sport)t);
            }
            F.Close();
            Console.WriteLine(" Информация выведена в файл.");
        }

        /// <summary>
        /// Выводит в разные файлы информацию о подписчиках на каждый разряд. Имя файла соответствует разряду.
        /// </summary>
        /// <param name="subs">Массив спортсмэнов.</param>
        public static void PrintInAFileAllCategories(Sportsman[] sportsmans)
        {
            for (int i = 0; i < 4; i++)
            {
                Category temp = (Category)i;
                string fileName = temp.ToString() + ".txt";
                PrintInAFileOneCategory(temp, sportsmans, CreateStreamWriter(fileName));
            }
        }
    }
}
