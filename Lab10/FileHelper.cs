﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Lab10
{
    class FileHelper
    {
        /// <summary>
        /// Открывает текстовый файл.
        /// </summary>
        /// <returns></returns>
        public static StreamReader OpenFile()
        {
            StreamReader F = null;
            bool exit = false;
            while (!exit)
            {
                try
                {
                    Console.Write("Введите имя файла: ");
                    string nameFile = Console.ReadLine();
                    F = new StreamReader(@nameFile, Encoding.Default);
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Файл с имнем \"" + nameFile + "\" успешно открыт.");
                    Console.ResetColor();
                    exit = true;
                }
                catch (FileNotFoundException)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("\nошибка: Файл с указанным именем не существует!\n");
                    Console.ResetColor();
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("\nошибка: " + e.Message + "\n");
                    Console.ResetColor();
                }
            }
            return F;
        }

        /// <summary>
        /// Создаёт список из строк с информацией для массива структур и закрывает поток
        /// </summary>
        /// <param name="F">Поток(файл), содержащий строки с информацией.</param>
        /// <returns></returns>
        public static List<string> ReadFromFile(StreamReader F)
        {
            List<string> res = new List<string>(); // список из считанных строк
            string buf;

            while ((buf = F.ReadLine()) != null)
                res.Add(buf);

            F.Close();

            return res;
        }


    }
}
