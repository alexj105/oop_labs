﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab10
{
    /// <summary>
    /// Класс содержит функции для организации меню.
    /// </summary>
    class ProgramHelper
    {
        /// <summary>
        /// Вывод всех факультетов ввиде меню.
        /// </summary>
        /// <param name="facs"></param>
        public static void OutputAllFacs(Faculty[] facs)
        {
            foreach (Faculty f in facs)
            {
                Console.WriteLine();
                f.OutputAll();
                Console.WriteLine();
            }
        }

        public static void MainMenu(Faculty[] facs)
        {
            bool exit = false;
            while (!exit)
            {
                Console.WriteLine("\n\nВыберите действие:");
                Console.WriteLine("1. Изменить название факультета.");
                Console.WriteLine("2. Добавить премию отличникам.");
                Console.WriteLine("3. Вывести на экран все факультеты.");
                Console.WriteLine("0. Выход.");
                int point = HandlerException.InputInteger("Здесь введите пункт: ");
                switch (point)
                {
                    case 1:
                        ProgramHelper.Menu1(facs);
                        Console.Write("\nДля продолжения нажмите любую клавишу . . . ");
                        Console.ReadKey();
                        break;
                    case 2:
                        ProgramHelper.Menu2(facs);
                        Console.Write("\nДля продолжения нажмите любую клавишу . . . ");
                        Console.ReadKey();
                        break;
                    case 3:
                        Console.WriteLine("\nВывод всех факультетов:");
                        ProgramHelper.OutputAllFacs(facs);
                        Console.Write("\nДля продолжения нажмите любую клавишу . . . ");
                        Console.ReadKey();
                        break;
                    case 0:
                        exit = true;
                        break;
                    default:
                        break;
                }
            }
        }

        public static void Menu1(Faculty[] facs)
        {
            Console.WriteLine("\nВыберите факультет:");
            for (int i = 0; i < facs.Length; i++)
                Console.WriteLine("{0}. {1}.", i + 1, facs[i].NameFac);
            int point = HandlerException.InputInteger("Здесь введите номер факультета: ");
            facs[point - 1].ChangingNameFac();
        }

        public static void Menu2(Faculty[] facs)
        {
            Console.WriteLine("\nВыберите факультет:");
            for (int i = 0; i < facs.Length; i++)
                Console.WriteLine("{0}. {1}.", i + 1, facs[i].NameFac);
            int point = HandlerException.InputInteger("Здесь введите номер факультета: ");
            facs[point - 1].AddingStipend();
        }
    }
}
