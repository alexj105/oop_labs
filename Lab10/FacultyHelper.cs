﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab10
{
    class FacultyHelper
    {
        /// <summary>
        /// Возвращает массив объектов класса Student, сформированный из данных, содержащиеся в списке data.
        /// </summary>
        /// <param name="data">Исходные данные.</param>
        /// <returns></returns>
        public static Faculty[] FormerArrayOfFaculties(List<string> data)
        {
            Faculty[] res = new Faculty[data.Count];

            for (int i = 0; i < data.Count; i++)
            {
                string[] buf = data[i].Split(';');

                res[i] = new Faculty(buf[0], buf[1], buf[2]);
            }
            return res;
        }
    }
}
