﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab10
{
    class Student
    {
        string lastName;
        string nameFac;
        int stipend;
        int[] marks;

        /// <summary>
        /// Конструктор с параметрами.
        /// </summary>
        /// <param name="lastName">Фамилия.</param>
        /// <param name="nameFac">Название факультета.</param>
        /// <param name="stipend">Стипендия.</param>
        /// <param name="marks">Оценки.</param>
        public Student(string lastName, string nameFac, int stipend, params int[] marks)
        {
            this.LastName = lastName;
            this.NameFac = nameFac;
            this.Stipend = stipend;
            this.Marks = marks;
        }

        /// <summary>
        /// Свойство для доступа к полю lastName
        /// </summary>
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        /// <summary>
        /// Свойство для доступа к полю nameFac
        /// </summary>
        public string NameFac
        {
            get { return nameFac; }
            set { nameFac = value; }
        }

        /// <summary>
        /// Свойство для доступа к полю stipend
        /// </summary>
        public int Stipend
        {
            get { return stipend; }
            set
            {
                if (value >= 0) stipend = value;
                else stipend = -1;
            }
        }

        /// <summary>
        /// Свойство для доступа к полю-массиву marks
        /// </summary>
        public int[] Marks
        {
            get { return marks; }
            set { marks = value; }
        }

        /// <summary>
        /// Определяет является ли студент, представляемый данным объектом, отличником.
        /// </summary>
        public bool IsExcellentPupil
        {
            get
            {
                if (this.AverageMark < 8)
                        return false;
                return true;
            }
        }

        /// <summary>
        /// Вычисляет средний бал.
        /// </summary>
        private double AverageMark
        {
            get
            {
                double x = 0;
                foreach (int i in Marks)
                    x += i;
                return x / Marks.Length;
            }
        }

        /// <summary>
        /// Выводит информацию о экзкмпляре в строку таблицы.
        /// </summary>
        /// <param name="numInTable">Номер строки в таблице.</param>
        public void OnOutput(object numInTable)
        {
            Console.WriteLine("│{0,3}│{1,18}│{2,11:f2}│{3,11}│", (int)numInTable, this.LastName, this.AverageMark, this.Stipend);
        }

        /// <summary>
        /// Изменяет название факультета.
        /// </summary>
        /// <param name="newNameFac">Новое название факультета.</param>
        public void OnChangeNameFac(object newNameFac)
        {
            this.NameFac = (string)newNameFac;
        }

        /// <summary>
        /// Увеличивает стипендию.
        /// </summary>
        /// <param name="addedStipend">Добавляемая премия.</param>
        public void OnAddStipend(object addedStipend)
        {
            this.Stipend += (int)addedStipend;
        }
    }
}
