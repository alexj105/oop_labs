﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab10
{
    class Faculty
    {
        /// <summary>
        /// Название факультета
        /// </summary>
        string nameFac;

        /// <summary>
        /// Фмилия декана
        /// </summary>
        string nameDean;

        /// <summary>
        /// Номер телефона
        /// </summary>
        string telephone;

        /// <summary>
        /// Поле-массив делегатов для хранения ссылок на методы OnOutput класса Student
        /// </summary>
        List<Del> outputMethods;

        /// <summary>
        /// Поле-делегат для хранения ссылок на методы OnChangeNameFac класса Student
        /// </summary>
        Del changeNameFacMethods;

        /// <summary>
        /// Поле-делегат для хранения ссылок на методы OnAddStipend класса Student
        /// </summary>
        Del addStipendMethods;

        /// <summary>
        /// Конструктор с параметрами.
        /// </summary>
        /// <param name="nameFac">Название факультета.</param>
        /// <param name="nameDean">Фамилия декана.</param>
        /// <param name="telephone">Номер телефона.</param>
        public Faculty(string nameFac, string nameDean, string telephone)
        {
            this.nameFac = nameFac;
            this.nameDean = nameDean;
            this.telephone = telephone;
            this.outputMethods = new List<Del>();
            this.changeNameFacMethods = null;
            this.addStipendMethods = null;
        }

        /// <summary>
        /// Свойство для доступа к полю nameFac
        /// </summary>
        public string NameFac
        {
            get { return nameFac; }
            set { nameFac = value; }
        }

        /// <summary>
        /// Свойство для доступа к полю nameDean
        /// </summary>
        public string NameDean
        {
            get { return nameDean; }
        }

        /// <summary>
        /// Свойство для доступа к полю telephone
        /// </summary>
        public string Telephone
        {
            get { return telephone; }
        }

        /// <summary>
        /// Регистрирует студента s на факультете.
        /// </summary>
        /// <param name="s">Регистрируеммый студент.</param>
        public void RegistrationOneStudent(Student s)
        {
            outputMethods.Add(new Del(s.OnOutput));
            changeNameFacMethods += new Del(s.OnChangeNameFac);
            if(s.IsExcellentPupil)
                addStipendMethods += new Del(s.OnAddStipend);
        }

        /// <summary>
        /// Выводит всех студентов данного факультета. Оповещает наблюдателей.
        /// </summary>
        public void OutputAll()
        {
            if (outputMethods.Count == 0)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine(" В факультете " + this.NameFac + " не содержится студентов!");
                Console.ResetColor();
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine(" {0}  {1}  {2}", this.NameFac, this.NameDean, this.Telephone);
                Console.ResetColor();
                Console.WriteLine("┌───┬──────────────────┬───────────┬───────────┐");
                Console.WriteLine("│ № │     Фамилия      │Средний бал│ Стипендия │");
                Console.WriteLine("├───┼──────────────────┼───────────┼───────────┤");
                for (int i = 0; i < outputMethods.Count; i++)
                    outputMethods[i]((object)(i + 1));
                Console.WriteLine("└───┴──────────────────┴───────────┴───────────┘");
            }
        }

        /// <summary>
        /// Изменяет название факультета. Оповещает наблюдателей.
        /// </summary>
        public void ChangingNameFac()
        {
            Console.WriteLine("\nТекущее название факультета: " + this.NameFac);
            Console.Write("Введите новое название факультета: ");
            this.NameFac = Console.ReadLine();
            if(changeNameFacMethods != null)
                changeNameFacMethods(this.NameFac);
            Console.WriteLine("Название факультета успешно изменено. Вывод:");
            this.OutputAll();
        }

        /// <summary>
        /// Добавляет премию отличникам. Оповещает наблюдателей.
        /// </summary>
        public void AddingStipend()
        {
            Console.WriteLine("\nВыбранный факультет: " + this.NameFac);
            int temp = HandlerException.InputInteger("Введите размер премии для отличников: ");
            if (addStipendMethods != null)
            {
                addStipendMethods(temp);
                Console.WriteLine("Премия успешно добавлена. Вывод:");
            }
            else
                Console.WriteLine("На этом факультете нету отличников. Вывод:");
            this.OutputAll();
        }
    }
}
