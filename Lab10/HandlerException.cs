﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab10
{
    class HandlerException
    {
        /// <summary>
        /// Безопасный ввод целого числа из стандартного потока.
        /// </summary>
        /// <param name="message">Выводимое сообщение</param>
        /// <returns></returns>
        public static int InputInteger(string message)
        {
            int result = 0;
            bool exit = false;
            while (!exit)
            {
                try
                {
                    Console.Write(message);
                    result = Convert.ToInt32(Console.ReadLine());
                    exit = true;
                }
                catch (FormatException)
                {
                    Console.WriteLine("ошибка ввода: Введите целое число!");
                }
                catch
                {
                    Console.WriteLine("неизвестная ошибка: Попробуйте ещё раз . . .");
                }
            }
            return result;
        }
    }
}
