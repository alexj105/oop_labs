﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab10
{
    class StudentHelper
    {
        /// <summary>
        /// Возвращает массив объектов класса Student, сформированный из данных, содержащиеся в списке data.
        /// </summary>
        /// <param name="data">Исходные данные.</param>
        /// <returns></returns>
        public static Student[] FormerArrayOfStudents(List<string> data)
        {
            Student[] res = new Student[data.Count];

            for (int i = 0; i < data.Count; i++)
            {
                string[] buf = data[i].Split(';');

                int[] bufMarks = new int[buf.Length - 3];       // формирование массива оценок
                for (int j = 0, t = 3; t < buf.Length; t++, j++)
                    bufMarks[j] = Convert.ToInt32(buf[t]);

                res[i] = new Student(buf[0], buf[1], Convert.ToInt32(buf[2]), bufMarks);
            }

            return res;
        }
    }
}
