﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab10
{
    class StudAndFacHelper
    {
        /// <summary>
        /// Регистрирует каждого студента из массива studs в одном из факкультетов массива facs.
        /// </summary>
        /// <param name="studs">Массив студентов.</param>
        /// <param name="facs">Массив факультетов.</param>
        public static void RegistrationOfStudsOnFacs(Student[] studs, Faculty[] facs)
        {
            for (int i = 0; i < studs.Length; i++)
            {
                bool exit = false;
                for (int t = 0; (t < facs.Length) && (!exit); t++)
                {
                    if (studs[i].NameFac == facs[t].NameFac)
                    {
                        facs[t].RegistrationOneStudent(studs[i]);
                    }
                }
            }
        }
    }
}
