﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab10
{
    /// <summary>
    /// Тип делегата для хранения ссылок на методы: OnOutput, OnChangeNameFac, OnAddStipend, которые описаны в 
    /// классе Student.
    /// </summary>
    /// <param name="i"></param>
    public delegate void Del(object obj);
    
    class Program
    {
        static void Main(string[] args)
        {
            // Формирование массивов из студентов и факультетов:
            Student[] studs = StudentHelper.FormerArrayOfStudents(FileHelper.ReadFromFile(FileHelper.OpenFile()));
            Faculty[] facs = FacultyHelper.FormerArrayOfFaculties(FileHelper.ReadFromFile(FileHelper.OpenFile()));

            StudAndFacHelper.RegistrationOfStudsOnFacs(studs, facs); // регистрация студенов на факультетах

            Console.WriteLine("Считанные данные из файлов:");
            ProgramHelper.OutputAllFacs(facs);

            ProgramHelper.MainMenu(facs);

            Console.Write("\nПрограмму написал студент группы ИТ-22 Савельев Александр.");
            Console.Write("\nДля выхода нажмите любую клавишу . . . ");
            Console.ReadKey();
        }
    }
}
